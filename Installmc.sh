#!/bin/bash
if [ $USER != 'minecraft' ]
then
	echo "Please run this script as the user 'minecraft'"
	exit 1
fi

if [ $# -ne 2 -a $# -ne 3 ]
then
	echo "Usage: Install [Name] [URL] <Port>"
	exit 1
fi

dir=$1; url=$2; port=$3
if [ -d ./Minecraft/$dir ]
then
	echo "Directory Exists. Exiting."
fi

mkdir ./Minecraft/$dir
cd ./Minecraft/$dir

curl -L $url > download
type=$(file -b download)
echo "Extracting..."

if [[ $(echo $type | grep "HTML") ]]
then
	echo "Check the download link. Recieved an HTML Document. Not an Archive."
	rm download
	cd ..
	rmdir ./$dir
	exit 1
elif [[ $(echo $type | grep "gzip") ]]
then
	tar -zxf download
elif [[ $(echo $type | grep "Zip archive") ]]
then
	unzip -q download
elif [[ $(echo $type | grep "POSIX tar") ]]
then
	tar -xf download
else
	echo "Unable to determine downloaded file type"
	exit 1
fi

rm download

if [ -d "download" ]
then
	mv ./download/* ./
	rmdir download
fi

if [ ! -f "eula.txt" ]
then
	echo "Creating eula..."
	echo "eula=true" > eula.txt	
else
	echo "Modifying eula..."
	sed -i "/eula=/ s/=.*/=true/" eula.txt
fi

echo "Setting port and MOTD..."
if [ ! -f "server.properties" ]
then
	
	echo "motd=$dir" >> server.properties
	
	if [[ $port ]]
	then
		echo "server-port=$port" > server.properties
	fi
else
	
	sed -i "s/\(motd=\).*\$/\1${dir}/" server.properties
	
	if [[ $port ]]
	then
 		sed -i "s/\(server-port=\).*\$/\1${port}/" server.properties
	fi
fi

echo "Adding server to startup config file...."
echo $dir >> ~minecraft/Servers/mcservers.cfg

if [ ! -f ServerStart.sh ]
then
	echo "Missing ServerStart.sh! The server will not start correctly. Please add one!"
	exit 1
fi

echo "Starting server..."
bash ~minecraft/Servers/Start.sh $dir

