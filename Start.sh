#!/bin/bash
mcservers=( "UltimateReloaded" )
declare -A ports
checkports() { 
	SERVER=$1
        DIR=/home/minecraft/Servers/Minecraft/$SERVER/
	
	if [ ! -d $DIR ]; then
		echo "Server $SERVER Does not exist; Try installing it using ./Install.sh"
		return 1
	fi
	PROPERTIES=server.properties
	PORT=$(grep 'server-port' $DIR$PROPERTIES | cut -d "=" -f2)
	RUNNING=$(lsof -i | grep $PORT)
	if [[ -z $RUNNING ]]; then
		echo "[$SERVER] no server bound to port $PORT yet"
		if [[ -z ${ports[$PORT]} ]]; then
			ports[$PORT]=$SERVER;
			return 0
		else
			echo "[$SERVER] Port $PORT has been claimed by ${ports[$PORT]} and is in the process of starting."
			return 1
		fi
	else
		echo "[$SERVER] There is a server already bound to port $PORT"
		return 1
	fi
	
}

startmc () {
	SERVER=$1
	DIR=/home/minecraft/Servers/Minecraft/$SERVER/
	SCRIPT=ServerStart.sh
	LOG=/var/www/mc/logs/MC_$SERVER.log
	SOCKET=/tmp/mcserv
	if [ -d $DIR ]; then
		if [ -f $DIR$SCRIPT ]; then
			echo "[$SERVER] Starting server..."
			tmux new -s MC_${1//./_} -d bash -c "bash -c 'cd $DIR && bash ./$SCRIPT | tee $LOG'"
			#chgrp minecraft $SOCKET
		else
			echo "[$1] Error: ServerStart.sh Missing!"
		fi 
	else
		echo "[$1] Error: Directory for server does not exist!"
	fi
}

if [ -z $1 ]; then
	while read -r srv; do
	#for srv in "${mcservers[@]}"; do
		if checkports $srv; then
			startmc $srv
		fi
	done < ./mcservers.cfg;
else
	if checkports $1; then
		startmc $1
	fi
fi


#screen -dmS MC_Vanilla_1.10 /home/bdkuhman/Servers/Minecraft/Vanilla_1.10/ServerStart.sh	#25565
#screen -dmS MC_VanillaPlus /home/bdkuhman/Servers/Minecraft/VanillaPlus/ServerStart.sh	#25566
#screen -dmS MC_TechnoFirma /home/bdkuhman/Servers/Minecraft/TechnoFirma/ServerStart.sh  #25564
#screen -dmS MC_InfSkyblock /home/bdkuhman/Servers/Minecraft/InfSkyblock/ServerStart.sh  #25567
#screen -dmS MC_Inventions /home/bdkuhman/Servers/Minecraft/Inventions/ServerStart.sh #25566
#screen -dmS MC_Vanilla_1.11 /home/bdkuhman/Servers/Minecraft/Vanilla_1.11/ServerStart.sh #25565
#screen -dmS MC_ThaumAdv /home/bdkuhman/Servers/Minecraft/ThaumAdv/ServerStart.sh #25566
#screen -dmS MC_Beyond /home/bdkuhman/Servers/Minecraft/Beyond/ServerStart.sh #25565
#screen -dmS MC_Vanilla_1.12.2 /home/bdkuhman/Servers/Minecraft/Vanilla_1.12.2/ServerStart.sh #25564

#screen -dmS MC_Pyramid bash /home/bdkuhman/Servers/Minecraft/Pyramid/ServerStart.sh #25565
#screen -dmS MC_Whatever bash /home/bdkuhman/Servers/Minecraft/Whatever/ServerStart.sh

#screen -dmS MC_Sevtech bash -c "cd /home/bdkuhman/Servers/Minecraft/Sevtech/ && bash ./ServerStart.sh" #25565

#screen -dmS MC_Revelation bash -c "cd /home/bdkuhman/Servers/Minecraft/Revelation/ && bash ./ServerStart.sh" #25566

#screen -dmS Terraria1 /home/bdkuhman/Servers/Terraria/ServerStart.sh #7777
#        screen -S Terraria1 -X stuff '2'`echo -ne '\015'`
#        screen -S Terraria1 -X stuff `echo -ne '\015'`
#        screen -S Terraria1 -X stuff `echo -ne '\015'`
#        screen -S Terraria1 -X stuff `echo -ne '\015'`
#        screen -S Terraria1 -X stuff `echo -ne '\015'`

#screen -dmS Terraria2 /home/bdkuhman/Servers/Terraria/ServerStart.sh #7778
#        screen -S Terraria2 -X stuff '3'`echo -ne '\015'`
#        screen -S Terraria2 -X stuff `echo -ne '\015'`
#        screen -S Terraria2 -X stuff '7778'`echo -ne '\015'`
#        screen -S Terraria2 -X stuff `echo -ne '\015'`
#        screen -S Terraria2 -X stuff `echo -ne '\015'`




